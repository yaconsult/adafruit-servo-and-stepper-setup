# README #

# Install and configure changes that need to be made to the raspberry pi
# to use Adafruit servo controller and stepper controller boards using
# Ansible configuration management.

# Start with the Raspberry Pi Jesse image from raspberrypi.org
# And install it to the SD card using their instructions.

# If you run out of space because raspian isn't using the entire SD card,
# run sudo raspi-config to expand the filesystem on the memory card.

https://www.raspberrypi.org/downloads/raspbian/

# Add the repository for debain “stretch” next release to get newer version of ansible:

sudo echo 'deb http://archive.raspbian.org/raspbian/ stretch main' | sudo tee --append /etc/apt/sources.list

# Update the package lists for the repositories:

sudo apt-get update

# Install the latest available version of ansible from stretch repo:

sudo apt-get install ansible/stretch

# When running ansible directly on the raspberry pi

# Tell ansible to run commands on the local host, ie, itself:

sudo echo 'localhost ansible_connection=local' | sudo tee --append /etc/ansible/hosts

# When running ansible remotely
# Leo only:
ssh-copy-id pi@raspberrypi

# Leo only: To remove old key, if above error:
ssh-keygen -R raspberrypi


# Get the list of updated packages and latest updates
sudo apt-get update && sudo apt-get upgrade

# Reboot the PI to be running the latest kernel and modules
# If nothing was installed in the previous step, this isn't necessary
sudo reboot

# Install the prerequisite python packages to use ansible
sudo apt-get install python-pip python3-pip git sshpass

# The version of ansible available for the pi is very old so we
# need to download and install the latest version

libffi-dev

sudo pip install paramiko PyYAML Jinja2 httplib2 six

# Now use pip to install the latest raspberry pi version of ansible
sudo pip install ansible

# Clone Leo's playbook files to the raspberry pi to be set up
cd ~/Leo
git clone https://yaconsult@bitbucket.org/yaconsult/adafruit-servo-and-stepper-setup.git


sudo nano /etc/ansible/hosts
# Add the following line to the end of the file and save it:
localhost ansible_connection=local

# Run the playbook
ansible-playbook playbook.yml

# If there are problems or errors, run the playbook in single-step mode:
ansible-playbook playbook.yml --step

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

test change for git updates
